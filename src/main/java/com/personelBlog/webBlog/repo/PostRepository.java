package com.personelBlog.webBlog.repo;

import com.personelBlog.webBlog.models.Post;
import org.springframework.data.repository.CrudRepository;

public interface PostRepository extends CrudRepository<Post, Long> {
}
